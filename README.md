STRATIFY is a MatLab software that allows to solve light scattering problems involving _stratified_ (multilayered) spherical particle.
STRATIFY is based on a powerful transfer-matrix method, which allows solving for electric and magnetic fields in any layer of spherical particle (including host medium) to get any desirable near- or far-field property.
A comprehensive summary of the method, underlying theory and application is freely available in  [OSA Continuum 3 (8), 2290 (2020)](https://www.osapublishing.org/osac/abstract.cfm?uri=osac-3-8-2290).

The latest release v1.1 is available [here](https://gitlab.com/iliarasskazov/stratify/-/releases/v1.1) and includes:

* absorption, scattering and extinction
* radiative and nonradiative decay rates of an electric dipole emitter located in any nonabsorbing shell, including host
* electric and magnetic near-fields in any shell, including host
* electric and magnetic energy density and total energy

For more details, see [wiki](https://gitlab.com/iliarasskazov/stratify/-/wikis/home)

**Acknowledgements**

We thank Lorenzo Pattelli, Hiroshi Sugimoto, and Andrei Galiautdinov for useful comments, bug reports, and performance improvements.
